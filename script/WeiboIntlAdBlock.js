const request_url = $request.url;
const response_body = $response.body;

if (request_url.includes("interface/sdk/sdkad.php")) {
  try {
    const json_str = response_body.substring(0, response_body.length - 2);
    const json_obj = JSON.parse(json_str);
    let body = json_obj;
    body.needlocation = false;
    body.show_push_splash_ad = false;
    body.background_delay_display_time = 6e6;
    body.lastAdShow_delay_display_time = 6e6;
    body.realtime_ad_video_stall_time = 0;
    body.realtime_ad_timeout_duration = 0;
    body.ads = [];
    $done({ body: JSON.stringify(body) + "OK" });
    console.log("sdkad");
  } catch (err) {
    $notification.post("Weibo Intl Script", "Error", err);
  }
} else {
  try {
    let body = JSON.parse(response_body);
    if (request_url.includes("open_app")) {
      body.data.uve_feed_ad = 0;
      body.data.uve_ad_scene = "";
      body.data.uve_hot_ad = 0;
      body.data.vip_ad_duration = 6e6;
      body.data.vip_title_ad = "";
      body.data.detail_banner_ad = [];
      body.data.close_ad_setting = {};
      body.data.background_preview = "";
      console.log("open_app");
    } else if (request_url.includes("get_coopen_ads")) {
      body.data.reserve_ad_ios_id = "";
      body.data.uve_ad_scene = "";
      body.data.ad_duration = 6e6;
      body.data.ad_list = [];
      body.data.ad_ios_id = null;
      body.data.pic_ad = [];
      body.data.ad_cd_interval = 6e6;
      body.data.reserve_app_ad_ios_id = "";
      body.data.display_ad = 0;
      body.data.gdt_video_ad_ios = [];
      body.data.app_ad_ios_id = null;
      console.log("get_coopen_ads");
    } else if (request_url.includes("user_center")) {
      body.data.cards.forEach((card) => {
        card.items = card.items.filter((item) => !["personal_vip", "personal_wallpaper"].includes(item.type));
      });
      console.log("user_center");
    }
    $done({ body: JSON.stringify(body) });
  } catch (err) {
    $notification.post("Weibo Intl Script", "Error", err);
  }
}
